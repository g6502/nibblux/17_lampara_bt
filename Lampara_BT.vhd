library ieee;

use ieee.std_logic_1164.all;

entity Lampara_BT is 
	port(	clk	: in	std_logic;
			rx		: in	std_logic;
			busy	: out	std_logic;
			rele	: out	std_logic
	);

end Lampara_BT;

architecture behave of Lampara_BT is
	
	signal byte	: std_logic_vector(7 downto 0);
	
begin
	
	u0	: entity work.UART_RX
		port map(	clk	=> clk,
						rx		=> rx,
						busy	=> busy,
						byte	=> byte);
	process(byte)
	begin
		if byte = x"41" then
			rele <= '1';
		elsif byte = x"42" then
			rele <= '0';
		end if;
	end process;
end behave;